const http = require('http');
const apiRoute = require('./routes/api/router');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use('/api', apiRoute);
app.use(express.static('public'))

const server = new http.Server(app);
const port = process.env.PORT || 3001;
server.listen(port, () => console.log(`App Server Listening at ${port}`));

module.exports = app;

