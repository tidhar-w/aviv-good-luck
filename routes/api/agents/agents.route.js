const AgentController = require("./agents.controller").AgentController;
const express = require('express');
const AgentRouter = express.Router();

AgentRouter.get('/getIsolatedAgents', (req, res) => {
    try {
        res.json(AgentController.getIsolatedAgents('brazil'));
    } catch (e) {
        console.log(e.message)
    }
});

module.exports = AgentRouter;
