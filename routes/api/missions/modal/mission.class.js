function Mission(country, address, date) {
    const assignAgents = new Map();
    this.date = new Date(date);
    this.fullAddress = country + address
    this.assignAgent = function (agentID) {
        assignAgents.set(agentID, agentID)
    }
}