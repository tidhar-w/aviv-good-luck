const MissionController = require("./missions.controller").MissionController;
const express = require('express');
const MissionRouter = express.Router();

MissionRouter.get('/getClosestFarthest', (req, res) => {
    try {
        const lant = req.query.lant;
        const long = req.query.long;
        const clossest = MissionController.getClossestMission(lant, long);
        const farthest = MissionController.getFarthest(lant, long);
        res.json({clossest: clossest, farthest: farthest})
    } catch (e) {
        console.log(e.message)
    }
});
module.exports = MissionRouter;
