const express = require('express');
const AgentRouter = require('./agents/agents.route');
const MissionRouter = require('./missions/missions.route');
const apiRoute = express.Router();

apiRoute.use('/agents/', AgentRouter);
apiRoute.use('/missions/',MissionRouter);


module.exports = apiRoute;



